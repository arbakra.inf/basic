CREATE TABLE the_user (
    organization_id integer NOT NULL,
    created timestamp  NOT NULL,
    email text NOT NULL,
    password text,
    PRIMARY KEY (email)
);

CREATE TABLE organization (
    id integer NOT NULL,
    created_by integer NOT NULL,
    created timestamp NOT NULL,
    PRIMARY KEY (id)
);

alter table the_user
  add constraint fk_user_org_pk FOREIGN KEY (organization_id) references organization(id);
