module Internal.Interlude (module X, module Internal.Interlude) where

import Prelude as X hiding (show, id, head, putStrLn, print, readFile)
import qualified Prelude
import Data.Proxy as X
import Data.Kind as X
import Data.Semigroup as X hiding (Min, Max)
import Data.Text as X (Text)
import Data.ByteString as X (ByteString)
import Control.Lens as X (makeLenses, (^.), (.~), (%~), (^?), view, lensRules)
import Data.String.Conv as X
import Data.Function as X hiding (id)
import Data.Char as X
import Data.List as X hiding (head, insert)
import Data.Maybe as X
import Debug.Trace as X
import Data.String as X (IsString(..))
import Data.Aeson as X
import Data.Ord as X (Down(..))
import Data.Coerce as X
import Data.Bifunctor as X
import GHC.TypeLits as X (TypeError)
import Control.Monad.State as X (evalState, get, modify')
import Control.Monad.IO.Class as X
import Control.Monad as X
import Data.Functor.Identity as X
import qualified Data.Text.IO as Text
import Data.Foldable as X

show :: (Show a, IsString c) => a -> c
show = fromString . Prelude.show

identity :: a -> a
identity = Prelude.id
{-# INLINE identity #-}

head :: [a] -> Maybe a
head = listToMaybe
{-# INLINE head #-}

unsafeHead :: [a] -> a
unsafeHead = Prelude.head
{-# INLINE unsafeHead #-}

putStrLn :: MonadIO m => String -> m ()
putStrLn = liftIO . Prelude.putStrLn

print :: (MonadIO m, Show a) => a -> m ()
print = liftIO . Prelude.print

putText :: MonadIO m => Text -> m ()
putText = putStrLn . toS

readFile :: FilePath -> IO Text
readFile = Text.readFile

foldl1Def :: (a -> a -> a) -> a -> [a] -> a
foldl1Def _ d [] = d
foldl1Def f _ as = foldl1' f as
