{-# LANGUAGE UndecidableInstances, AllowAmbiguousTypes #-}
module Internal.Data.Basic.Compare where

import Internal.Interlude

import Internal.Data.Basic.Types
import Internal.Data.Basic.Sql.Types
import Database.PostgreSQL.Simple.ToField (ToField)
import Internal.Data.Basic.Lens
import Internal.Data.Basic.Common
import GHC.TypeLits

class ComparableInDbExp (a :: *) (b :: *) where
    compareInDbExp :: Comparison -> a -> b -> ConditionExp
instance {-# INCOHERENT #-} (ValueAsDbExp b a, Ord a)
    => ComparableInDbExp (DbExp k a) b where
    compareInDbExp comp a b = Compare comp a (valueAsDbExp b)
instance {-# INCOHERENT #-} (ValueAsDbExp a b, Ord b)
    => ComparableInDbExp a (DbExp k b) where
    compareInDbExp comp a = Compare comp (valueAsDbExp a)

class CompareNullable a b where
    nullableEq ::
        ( TableFieldType t1 f1 ~ a, TableFieldType t2 f2 ~ b
        , TableField t1 f1, TableField t2 f2 )
        => proxy f1 -> proxy f2
        -> Entity ('FromDb c) t1 -> Var 'Filtering t2 -> ConditionExp

instance
    (ComparableInDbExp (DbExp 'LiteralExp (Maybe a)) (Maybe b), a ~ b, Ord b, ToField b)
    => CompareNullable (Maybe a) (Maybe b) where
    nullableEq (_ :: proxy f1) (_ :: proxy f2) toTable fromTable =
        Literal (toTable ^. fieldOptic @f1) ==. fromTable ^. fieldOptic @f2

instance {-# OVERLAPPABLE #-}
    (ComparableInDbExp (DbExp 'LiteralExp (Maybe a)) (Maybe b), a ~ b, Ord b, ToField b)
    => CompareNullable (Maybe a) b where
    nullableEq (_ :: proxy f1) (_ :: proxy f2) toTable fromTable =
        case toTable ^. fieldOptic @f1 of
            Nothing -> dfalse
            Just f -> Literal f ==. fromTable ^. fieldOptic @f2

instance {-# OVERLAPPABLE #-}
    (ComparableInDbExp (DbExp 'LiteralExp (Maybe a)) (Maybe b), a ~ b, Ord b, ToField b)
    => CompareNullable a (Maybe b) where
    nullableEq (_ :: proxy f1) (_ :: proxy f2) toTable fromTable =
        Literal (Just (toTable ^. fieldOptic @f1)) ==. fromTable ^. fieldOptic @f2

instance {-# OVERLAPPABLE #-}
    (ComparableInDbExp (DbExp 'LiteralExp a) b, a ~ b, Ord b, ToField b)
    => CompareNullable a b where
    nullableEq (_ :: proxy f1) (_ :: proxy f2) toTable fromTable =
        Literal (toTable ^. fieldOptic @f1) ==. fromTable ^. fieldOptic @f2

fieldMatch :: forall (toField :: Symbol) (fromField :: Symbol) toTable fromTable c.
    ( CompareNullable (TableFieldType toTable toField) (TableFieldType fromTable fromField)
    , TableField toTable toField, TableField fromTable fromField )
    => Entity ('FromDb c) toTable -> Var 'Filtering fromTable -> ConditionExp
fieldMatch =
    nullableEq (Proxy @toField) (Proxy @fromField)

infix 4 ==.
infix 4 >.
infix 4 /=.
infix 4 <.
infix 4 <=.
infix 4 >=.
infixr 3 &&.
infixr 2 ||.

(>.), (==.), (/=.), (<.), (<=.), (>=.) :: ComparableInDbExp a b => a -> b -> ConditionExp
(>.) = compareInDbExp GreaterThan
(==.) = compareInDbExp Equal
(/=.) = compareInDbExp NotEqual
(<.) = compareInDbExp LessThan
(<=.) = compareInDbExp LessOrEqual
(>=.) = compareInDbExp GreaterOrEqual

(&&.) :: ConditionExp -> ConditionExp -> ConditionExp
(&&.) = BoolOp And

(||.) :: ConditionExp -> ConditionExp -> ConditionExp
(||.) = BoolOp Or
