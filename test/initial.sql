CREATE SCHEMA public;

CREATE TABLE user_session (
    id SERIAL NOT NULL,
    expiration_date timestamp NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE "user" (
    name text
);

CREATE TABLE test (
    id int not null,
    primary key (id)
);

CREATE TABLE capstable (
    "UPPER" int,
    "wEiRd" int
);

CREATE TABLE lower (
    "upper" int
);
