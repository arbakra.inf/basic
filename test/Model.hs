{-# LANGUAGE TemplateHaskell, DataKinds, TypeFamilies, FlexibleContexts, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Model where

import Data.Basic

mkFromFile "test/initial.sql"
