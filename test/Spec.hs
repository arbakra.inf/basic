{-# LANGUAGE DataKinds, NoImplicitPrelude, TypeFamilies, FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses, FlexibleContexts, OverloadedStrings #-}
import Prelude hiding (id)

import Data.Time
import Database.PostgreSQL.Simple

import Data.Basic
import Control.Monad.IO.Class
import Control.Lens
import Control.Monad
import Data.Function ((&))
import Data.Aeson
import Data.String.Conv (toS)

import Model

insertNewSession :: MonadIO m => Connection -> LocalTime -> m ()
insertNewSession conn expiration =
    handleBasicPsql conn query'
    where
        session = newUserSession
            & expirationDate .~ expiration
            & id .~ 0
        query'  = void $ insert session

main :: IO ()
main = do
    conn <- connectPostgreSQL "host=localhost port=5432 user=postgres dbname=test password=admin connect_timeout=10"
    handleBasicPsql conn $ do
        executeQuery "drop schema if exists public cascade;" ()
        applySchema initialSchema
        us <- allUsers
        liftIO $ putStrLn (toS $ encode us)
        _ <- dmap (view upper) allCapstables
        void $ dmap (view upper) allLowers
